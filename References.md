# References
* [EPA PM AQI Color](https://www.airnow.gov/index.cfm?action=pubs.aqguidepart)
* [AQIcn](https://aqicn.org/city/beijing/)
* [InstantCast vs NowCast](http://aqicn.org/faq/2015-03-15/air-quality-nowcast-a-beginners-guide/)
* [Computing AQI](https://en.wikipedia.org/wiki/Air_quality_index#Computing_the_AQI)
* [MAP web services using LEAFLET](http://aqicn.org/faq/2015-09-18/map-web-service-real-time-air-quality-tile-api/) 