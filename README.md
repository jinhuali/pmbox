# README #
* ![Box_img1](image/2017-03-01_20-32-18_915.jpeg)
* ![Box_img2](image/2017-03-01_20-31-05_662.jpeg)
* ![Screenshot](image/Screenshot_2017-01-11-22-28-37.jpg)

## pmclient.py: desktop client for communication with arduino. ##
* Description: Arduino pmbox pc client. functions include: (1) display real-time pm PM readings in GUI, (2) save the readings to local HD, (3) upload pm data log files from SD card to PC
* Version: 1.0
* Usage: python3 pmclient.py
* Python_version: 3.0.0 and above
* OS: tested on ubuntu

## pmbox_mega.ino: arduino sketch file. ##
* SPI connection with Arduino Mega: 

   1. Micro sd card:
     * MOSI - pin 51
     * MISO - pin 50
     * CLK - pin 52
     * CS - pin 53
   2. DS3231:  
     * SDA - pin 20 
     * SCL - pin 21 
   3. 0.95 Oled display (bw):
     * D0(CLK) - pin 7
     * D1(MOSI) - pin 6
     * CS - pin 5
     * DC - pin 4
     * RES - RESET
   4. DHT11
     * out - pin 8
   5. 6MM red green bicolor 3 pin LED
     * RED - pin 9
     * GREEN - pin 10
   7. LDR
     * value - pin A0
   8. GPS
     * TX - RX1
   9. PM Sensor
     * RX - TX2
     * TX - RX2 
  10. HC-06
     * RX - TX3
     * TX - RX3
     
## app-release-unsigned.apk: android app "EcoDuino" ##
* display real time arduino sensor readings through bluetooth connection between Android and Arduino
* install: first copy the apk file into your android phone local folder; then double click the apk file and follow the installation process