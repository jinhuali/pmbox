# !/usr/bin/env python3
# title           :pmclient-r1.py
# description     :Arduino UNO pm sensor ubuntu client. functions include: (1) display real-time pm PM readings in GUI, (2) save the readings to local HD,
# author          :j
# date            :2016.10.31
# version         :0.2
# usage           :python3 pmclient-r1.py
# notes           :
# python_version  :3.0.0 and above
# ==============================================================================

from tkinter import *
import serial.tools.list_ports
import time
import tkinter.filedialog
import tkinter.messagebox
import os

#  list available serial ports
def serial_ports():
    for port in list(serial.tools.list_ports.comports()):
        yield port[0]

OPTIONS = list(serial_ports())


class MyGUI:
    def __init__(self, master):
        self.master = master
        self.master.minsize(width=200, height=100)
        master.title("PM sensor")

        ports = list(serial.tools.list_ports.comports())
        Label(master, text='Choose a device').grid(row = 0, column = 0)
        self.var_optionmenu = StringVar(master)
        self.var_optionmenu.set(OPTIONS[0]) #  initial value
        OptionMenu(master, self.var_optionmenu, *OPTIONS).grid(row = 0, column = 1)
        # option.pack()

        self.dir = StringVar()
        self.dir.set(os.getcwd())
        Button(master, text="Choose a directory", command=self.askdirectory).grid(row=1, column=0)
        Label(master, textvariable=self.dir).grid(row=1, column=1)
        self.fo = open(self.dir.get()+'/pmlog.txt', 'a')

        self.var_label1 = StringVar(master)
        self.var_label1.set("Display current time")
        Label(master, textvariable=self.var_label1, fg = "yellow", font = "Helvetica 16 bold italic").grid(row = 2, column = 0, columnspan = 3)

        self.var_label2 = StringVar(master)
        self.var_label2.set("Display PM value")
        Label(master, textvariable=self.var_label2, fg="light green" if root.pm25<100 else "red", bg="dark green", font="Helvetica 16 bold italic").grid(row = 3, column = 0, columnspan = 3)

        self.button1 = Button(master, text="connect", command=self.connect)
        self.button1.grid(row = 4, column = 1)
        self.button2 = Button(master, text="pause", command=self.pause)
        self.button2.grid(row = 4, column = 2)
        self.button3 = Button(master, text="upload", command=self.upload)
        self.button3.grid(row = 4, column = 0)

        self.button2['state'] = 'disabled'
        self.button3['state'] = 'disabled'
    # set file directory where to save pm data log file
    def askdirectory(self):
        self.fo.close()
        self.dir.set(tkinter.filedialog.askdirectory(parent=root, initialdir="~", title="choose a directory to save PM data"))
        self.fo = open(self.dir.get()+'/pmlog1.txt', 'a')

    # begin to read pm data from the sensor

    def connect(self):
        self.button1['state'] = 'disabled'
        self.button2['state'] = 'normal'
        if tkinter.messagebox.askyesno("upload", "Upload files from SD card?"):
            self.upload()
        self.arduinoport = self.var_optionmenu.get()
        self.ser_ACM = serial.Serial(self.arduinoport, 9600)
        self.pmread()
        # fo.close()
        # ser_ACM.close()

    # upload pm log data from sd card to the pc
    def upload(self):
        self.arduinoport = self.var_optionmenu.get()
        self.ser_ACM = serial.Serial(self.arduinoport, 9600)
        time.sleep(1)
        self.ser_ACM.write(b'101') # request for connection
        self.var_label1.set("Request connection ...")
        root.update_idletasks()
        while 1: # wait for Arduion confirmation
            s1 = self.ser_ACM.readline().decode("utf-8").rstrip()
            print(s1)
            if(s1 == "101OK"):
                self.var_label1.set("Connection granted")
                break;
        self.var_label1.set("Uploading ...")
        while 1:  #  wait for Arduion confirmation
            s1 = self.ser_ACM.readline().decode("utf-8").rstrip()
            if (s1.rstrip() == "101END"):
                try:
                    f
                except NameError:
                    print('do nothing')
                else:
                    f.close()
                break;
            if s1[:2]=='PM':
                try:
                    f
                except NameError:
                    print('do nothing')
                else:
                    f.close()
                self.var_label1.set("Uploading from " + s1 + " ...")
                f = open(self.dir.get()+"/"+s1, 'w+')
            else:
                f.write(s1)
                f.write("\n")
                self.var_label2.set(s1)

            root.update_idletasks()

        self.var_label1.set("Uploading completed")
        self.ser_ACM.close()

    # pause reading
    def pause(self):
        root.isreading = not root.isreading
        self.button1['state'] = 'normal'
        self.button2['state'] = 'disabled'

    # get current geolocation in latitude and longitue
    def getgeolocation(self):
        return [0.0, 0.0, 0.0]

    # read pm data
    def pmread(self):
        if root.isreading:
            s1 = self.ser_ACM.readline().decode("utf-8")
            ss1 = s1.split(':')
            if ss1[0].strip() == 'pm25':
                root.pm25 = int(ss1[1])
                s2 = self.ser_ACM.readline().decode("utf-8")
                ss2 = s1.split(':')
                if ss2[0].strip() == 'pm10':
                    root.pm10 = int(ss2[1])
                nowtime = int(time.time())
                tmstr = time.strftime("%Y-%m-%d %H:%M", time.localtime(nowtime))
                pmstr = 'Pm2.5=' + str(root.pm25) + 'ug/m3'
                print(pmstr)
                self.var_label1.set(tmstr)
                self.var_label2.set(pmstr)
                root.update_idletasks()
                loc = self.getgeolocation()
                self.fo.write(str(nowtime) + ',' + str(loc[0]) + ',' + str(loc[1]) + ',' + str(root.pm25) + ',' + str(root.pm10) + '\n')
                self.fo.flush()
            root.after(100, self.pmread())
        else:
            root.isreading = True



def callback():
    if tkinter.messagebox.askokcancel("Quit", "Do you really wish to quit?"):
        root.destroy()

root = Tk()
root.isreading = True
root.pm25 = 0
root.pm10 = 0
my_gui = MyGUI(root)
root.protocol("WM_DELETE_WINDOW", callback)
root.mainloop()