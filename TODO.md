##Hardware:
* **HIGH** packaging (3D printing): 
    * core: arduino UNO + pm sensor + gps + microSD + temperature/humidity + bluetooth + status LEDs (**Update** design iteration 1 under progress 2/6/17)
    * standard: arduino Mega + pm sensor + OLED(color/bw) + gps + microSD + temperature/humidity + RTC + bluetooth + status LEDs 
* **MEDIUM** upgrade dht11 to dht22. much improved accuracy
* replace bluetooth hc06 to hc05. better compatibility (**Update** not required 1/11/2017)

##Features to be added:
* **MEDIUM** increase baute rate on the fly to reduce log data upload time from microsd card to the pc.
* bluetooth and smartphone app. ref to SensoDuino (**Update** ver0.1 1/10/2017)
* **LOW** outdoor and indoor mode switch. different display setting. 
* **MEDIUM** air quality index (AQI)
* **LOW** pm value predictor
* **LOW** cloud storage

##Others
* **LOW** live air pollute + pmBox promotion video 
* **HIGH** pmBox_UNO.ino
* **HIGH** LOGO
* lithium battery

##Problems:
* test liudu pin 6 (SET) [low (0v): stop and high(3.3v) or empty: run]. tried to connect pin 6 to uno/mega digital pin. not working as expected. liudu stops runing (fan and laser?) as expected while setting the uno digital pin output on LOW. however, the output reading from liudu output is off nominal range while setting pin output on HIGH. is it because of voltage (5V vs 3.3V)? (**Solution 1**: connect 1k and 2k resistors in series 1/5/2017)
* UNO multiple SoftwareSerial (**Solved** 12/13/16)