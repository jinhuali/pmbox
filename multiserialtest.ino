/********************
1.Put U8glib in arduino library directory
2.Open the file  U8gLogo
3.Connection mode

Adruino    = OLED
12    = SCL
11    = SDA
10  = CS
9   = DC
REST    = RES

4.Compile and download
****************************/
#
#include <SoftwareSerial.h>  
#include <TinyGPS.h>

TinyGPS gps;

SoftwareSerial mySerial_panteng(4, 5);
SoftwareSerial mySerial_gps(9,8);
int Pm25_cn = 0;
int Pm25_us = 0;
int Pm10 = 0;


char CopeSerialData(unsigned char ucData)
{
  static unsigned char ucRxBuffer[50];
  static unsigned char ucRxCnt = 0;
  int  pmcf10=0;
  int  pmcf25=0;
  int  pmcf100=0;
  int  pmat10=0;
  int  pmat25=0;
  int  pmat100=0;
  int  pmcount03=0;
  int  pmcount05=0;
  int  pmcount10=0;
  int  pmcount25=0;
  int  pmcount50=0;
  int  pmcount100=0;

  
  ucRxBuffer[ucRxCnt++]=ucData;

  if (ucRxBuffer[0]!=0x42&&ucRxBuffer[1]!=0x4D) 
  {
    ucRxCnt=0;
    return ucRxCnt;
  }
  if (ucRxCnt<32) {return ucRxCnt;}
  else
  {
    for (int i=0;i<32;i++)
    {Serial.print(ucRxBuffer[i]);Serial.print("  ");}
    Serial.println(""); 
    pmcf10=(int)ucRxBuffer[4]*256+(int)ucRxBuffer[5];Serial.print("PM1.0_CF1:");Serial.print(pmcf10);Serial.print("   "); //CF=1 根据美国TSI公司的仪器校准
    pmcf25=(int)ucRxBuffer[6]*256+(int)ucRxBuffer[7];Serial.print("PM2.5_CF1:");Serial.print(pmcf25);Serial.print("   ");
    pmcf100=(int)ucRxBuffer[8]*256+(int)ucRxBuffer[9];Serial.print("PM10_CF1:");Serial.print(pmcf100);Serial.println("   ");
    pmat10=(int)ucRxBuffer[10]*256+(int)ucRxBuffer[11];  Serial.print("PM1.0_AT:");Serial.print(pmat10);Serial.print("   "); //大气环境下 根据中国气象局的数据校准
    pmat25=(int)ucRxBuffer[12]*256+(int)ucRxBuffer[13];  Serial.print("PM2.5_AT:");Serial.print(pmat25);Serial.print("   ");
    pmat100=(int)ucRxBuffer[14]*256+(int)ucRxBuffer[15];  Serial.print("PM10_AT:");Serial.print(pmat100);Serial.println("   ");
    pmcount03=(int)ucRxBuffer[16]*256+(int)ucRxBuffer[17];  Serial.print("PMcount0.3:");Serial.print(pmcount03);Serial.print("   ");
    pmcount05=(int)ucRxBuffer[18]*256+(int)ucRxBuffer[19];  Serial.print("PMcount0.5:");Serial.print(pmcount05);Serial.print("   ");
    pmcount10=(int)ucRxBuffer[20]*256+(int)ucRxBuffer[21];  Serial.print("PMcount1.0:");Serial.print(pmcount10);Serial.println("   ");
    pmcount25=(int)ucRxBuffer[22]*256+(int)ucRxBuffer[23];  Serial.print("PMcount2.5:");Serial.print(pmcount25);Serial.print("   ");
    pmcount50=(int)ucRxBuffer[24]*256+(int)ucRxBuffer[25];  Serial.print("PMcount5.0:");Serial.print(pmcount50);Serial.print("   ");
    pmcount100=(int)ucRxBuffer[26]*256+(int)ucRxBuffer[27];  Serial.print("PMcount10:");Serial.print(pmcount100);Serial.println("   ");
    Serial.println(" *****************************************************************  ");
    Serial.println("   ");
    Pm25_us=pmcf25;
    Pm25_cn=pmat25;
    Pm10=pmcf10;
    if (Pm25_cn > 9999)
      Pm25_cn = 9999;
    if (Pm25_us > 9999)
      Pm25_us = 9999;
    ucRxCnt=0;
    return ucRxCnt;
    }
}

void ProcessSerialData_Panteng(){
    // For one second we parse GPS data and report some key values
  for (unsigned long start = millis(); millis() - start < 1000;)
  {
    while (mySerial_panteng.available())
    {
      char c = mySerial_panteng.read();
      Serial.print(c, DEC);
      Serial.print(" ");
      CopeSerialData(c);
    }
  }
  Serial.println();
}

void setup(void) {
  // Open serial communications and wait for port to open:  
  Serial.begin(9600);  
  while (!Serial) {  
    ; // wait for serial port to connect. Needed for Leonardo only  
  }  
  Serial.println("Start");
  
  // set the data rate for the SoftwareSerial port  
  mySerial_panteng.begin(9600);
  mySerial_gps.begin(9600);
}

void ProcessSerialData_gps(){
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;

  // For one second we parse GPS data and report some key values
  for (unsigned long start = millis(); millis() - start < 1000;)
  {
    while (mySerial_gps.available())
    {
      char c = mySerial_gps.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)) // Did a new valid sentence come in?
        newData = true;
    }
  }

  if (newData)
  {
    float flat, flon;
    unsigned long age;
    gps.f_get_position(&flat, &flon, &age);
    Serial.print("LAT=");
    Serial.print(flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat, 6);
    Serial.print(" LON=");
    Serial.print(flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon, 6);
    Serial.print(" SAT=");
    Serial.print(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
    Serial.print(" PREC=");
    Serial.print(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
  }
  
  gps.stats(&chars, &sentences, &failed);
  Serial.print(" CHARS=");
  Serial.print(chars);
  Serial.print(" SENTENCES=");
  Serial.print(sentences);
  Serial.print(" CSUM ERR=");
  Serial.println(failed);
  if (chars == 0)
    Serial.println("** No characters received from GPS: check wiring **");  
}

void loop(void) {
  mySerial_panteng.listen();
  Serial.println("Data from panteng:");
  ProcessSerialData_Panteng();
  mySerial_gps.listen();
  Serial.println("Data from gps:");
  ProcessSerialData_gps();
  // rebuild the picture after some delay
  //delay(5*1000); // delay 5 seconds
}

