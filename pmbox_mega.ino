
#include <U8glib.h>
#include <Time.h>
//#include <SoftwareSerial.h>
#include <DS3231.h>
#include <DHT.h>
#include <TinyGPS.h>

TinyGPS gps;
#define GpsSerial Serial1
#define PmSerial Serial2
#define BTSerial Serial3

#define DHTPIN 8        //define as DHTPIN the Pin 13 used to connect the Sensor
#define DHTTYPE DHT11    //define the sensor used(DHT11)
DHT dht(DHTPIN, DHTTYPE);//create an instance of DHT

/* Micro sd card
  The circuit:
   SD card attached to SPI bus as follows:
   for MEGA
 ** MOSI - pin 51
 ** MISO - pin 50
 ** CLK - pin 52
 ** CS - pin 53*/
#include <SPI.h>
#include <SD.h>

const int chipSelect = 53;

// Arduino Uno:
// ----------------------
// DS3231:  SDA pin   -> Arduino Analog 20 or the dedicated SDA pin
//          SCL pin   -> Arduino Analog 21 or the dedicated SCL pin
DS3231  rtc(SDA, SCL);

// 0.95 Oled display (bw)
U8GLIB_SSD1306_128X64 u8g(7, 6, 5, 4);  // SW SPI Com: D0(CLK) = 7, D1(MOSI) = 6, CS = 5, DC = 4, RES=RESET

// PM sensor
//SoftwareSerial Serial2(2, 3); // RX = 3, TX = 2


int LDRPin = A0; // select the input pin for ldr
int LDRValue = 0; // variable to store the value coming from the sensor

int Pm25 = 0;
int Pm10 = 0;
Time tm_cur; //local time
long timestamp_rtc, timestamp_gps, timestamp_local;
int humid = 0;
int temp = 0;
int pollutelevel = 0;
char pollutedesc[10];
float loc[3] = {0.0, 0.0, 0.0};
boolean DELOLDFILE = false; //delete log file after upload to pc?

const int GREEN = 9;
const int RED = 10;
const int PMSET = 11;
boolean HIBERNATE = false;
const int tmzone = -8; // california pacific
// int tmzone = +8; // china

void ProcessSerialData_liudu()
{
  uint8_t mData = 0;
  uint8_t i = 0;
  uint8_t mPkt[8] = {0};
  uint8_t mCheck = 0;
  uint8_t mAva = 0;

  while (PmSerial.available() > 0)
  {
    //for liudu
    //packet format: AA  PM25_H PM25_L PM10_H  PM10_L DC FF
    delay(500);
    mData = PmSerial.read();     delay(2);//wait until packet is received
    //Serial.print("full header: "); Serial.println(mData);

    if (mData == 0xAA) //head ok
    {
      //Serial.println("header is ok and now reading:");
      mPkt[0] =  mData;
      mCheck = 0;

      for (i = 0; i < 4; i++) //data recv
      {

        mPkt[i + 1] = PmSerial.read();  delay(2);
        //Serial.print(i + 1); Serial.print(": "); Serial.println(mPkt[i + 1]);
        mCheck += mPkt[i + 1];
      }

      mPkt[5] = PmSerial.read();
      delay(2);
      mPkt[6] = PmSerial.read();
      delay(2);
      //Serial.print("Ending: "); Serial.println(mPkt[6]);

      if (mCheck == mPkt[5]) //crc ok
      {
        PmSerial.flush();
        //Serial.println("Clear  Clear Clear#######################");
        Pm25 = ((uint16_t)mPkt[2] | (uint16_t)(mPkt[1] << 8)) / 10;
        Pm10 = ((uint16_t)mPkt[4] | (uint16_t)(mPkt[3] << 8)) / 10;
        if (Pm25 > 9999)
          Pm25 = 9999;
        if (Pm10 > 9999)
          Pm10 = 9999;
        //get one good packet

        if (Pm25 < 35) {
          pollutelevel = 0;
        } else if (Pm25 < 75) {
          pollutelevel = 1;
        } else if (Pm25 < 115) {
          pollutelevel = 2;
        } else if (Pm25 < 150) {
          pollutelevel = 3;
        } else if (Pm25 < 250) {
          pollutelevel = 4;
        } else {
          pollutelevel = 5;
        }
      } else {
        Serial.println("crc error");
      }
    }
  }
}


void draw() {
  // graphic commands to redraw the complete screen should be placed here
  //u8g.setFont(u8g_font_fur11);

  char strbuffer[10];
  switch (tm_cur.dow) {
    case 1:
      sprintf(strbuffer, "%s", "Mon.");  break;
    case 2:
      sprintf(strbuffer, "%s", "Tue.");  break;
    case 3:
      sprintf(strbuffer, "%s", "Wed.");  break;
    case 4:
      sprintf(strbuffer, "%s", "Thu.");  break;
    case 5:
      sprintf(strbuffer, "%s", "Fri.");  break;
    case 6:
      sprintf(strbuffer, "%s", "Sat.");  break;
    case 7:
      sprintf(strbuffer, "%s", "Sun.");  break;
    default:
      break;
  }
  u8g.setFont(u8g_font_gdr12);
  u8g.setPrintPos(0, 15);
  u8g.print(strbuffer);
  sprintf(strbuffer, "%d:%d", tm_cur.hr, tm_cur.min);
  u8g.setPrintPos(90, 15);
  u8g.print(strbuffer);
  sprintf(strbuffer, "%d-%d", tm_cur.mon, tm_cur.date);
  u8g.setPrintPos(40, 15);
  u8g.print(strbuffer);
  u8g.setPrintPos(10, 40); //set position
  u8g.print("PM2.5");

  //u8g.setFont(u8g_font_tpssb);
  u8g.setPrintPos(0, 64); //set position
  sprintf(strbuffer, "%dC", int(temp));
  u8g.print(strbuffer);
  u8g.setPrintPos(100, 64); //set position
  sprintf(strbuffer, "%d%%", int(humid));
  u8g.print(strbuffer);

  switch (pollutelevel) {
    case -1:
      sprintf(strbuffer, "%s", "HIBER"); break;
    case 0:
      sprintf(strbuffer, "%s", "GOOD"); break;
    case 1:
      sprintf(strbuffer, "%s", "MOD"); break;
    case 2:
      sprintf(strbuffer, "%s", "UnHLTY1"); break;
    case 3:
      sprintf(strbuffer, "%s", "UnHLTY2"); break;
    case 4:
      sprintf(strbuffer, "%s", "UnHLTY3"); break;
    case 5:
      sprintf(strbuffer, "%s", "HAZARD"); break;
    default:
      break;
  }
  u8g.setPrintPos(30, 64); //set position
  u8g.print(strbuffer);

  u8g.setFont(u8g_font_fub20);
  u8g.setPrintPos(70, 45);
  u8g.print(Pm25);
}

boolean haveSD = true;
void setup(void) {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serial.println("Start");

  // set the data rate for the SoftwareSerial port
  PmSerial.begin(9600);
  Pm25 = 0;
  Pm10 = 0;

  GpsSerial.begin(9600);
  BTSerial.begin(9600);

  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    haveSD = false;
  } else {
    Serial.println("card initialized.");
    SD.mkdir("pmbox");
  }
  dht.begin();
  rtc.begin();

  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);

  pinMode(PMSET, OUTPUT);
  // The following lines can be uncommented to set the date and time
  //rtc.setDOW(FRIDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(0, 51, 0);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(11, 11, 2016);   // Set the date to January 1st, 2014

  // assign default color value
  if ( u8g.getMode() == U8G_MODE_R3G3B2 ) {
    u8g.setColorIndex(255);     // white
  }
  else if ( u8g.getMode() == U8G_MODE_GRAY2BIT ) {
    u8g.setColorIndex(3);         // max intensity
  }
  else if ( u8g.getMode() == U8G_MODE_BW ) {
    u8g.setColorIndex(1);         // pixel on
  }
  else if ( u8g.getMode() == U8G_MODE_HICOLOR ) {
    u8g.setHiColorByRGB(255, 255, 255);
  }
}

/*void getgeolocation(float *loc){

  }*/

void datalogger() {
  // make a string for assembling the data to log:
  String dataString = "";
  //getgeolocation(loc); // obtain gps location
  // make a string for assembling the data to log:
  // format: unixtime, latitude, longitude, altitude, Pm2.5(ug/m3), Pm10(ug/m3), temparture(C), relative humidity
  dataString += String(timestamp_rtc);
  dataString += ",";
  dataString += String(loc[0], 5);
  dataString += ",";
  dataString += String(loc[1], 5);
  dataString += ",";
  dataString += String(loc[2]);
  dataString += ",";
  dataString += String(Pm25);
  dataString += ",";
  dataString += String(Pm10);
  dataString += ",";
  dataString += String(temp);
  dataString += ",";
  dataString += String(humid);
  //Serial.print("log data: "), Serial.println(dataString);

  String fname = String();
  fname = "/pmbox/pm";
  fname += String(tm_cur.yr % 100);
  fname += String(tm_cur.mon);
  fname += String(tm_cur.date);
  fname += ".txt";
  char cfname[fname.length() + 1];
  fname.toCharArray(cfname, sizeof(cfname));
  //Serial.println(cfname);
  File dataFile = SD.open(cfname, FILE_WRITE);
  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.print("logdata"), Serial.print(": "), Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
  }
}

void readtime() {
  tm_cur = rtc.getTime();
  timestamp_local = rtc.getUnixTime(tm_cur);
  timestamp_rtc = timestamp_local - tmzone * 3600;
}

void readtemphumid() {
  humid = dht.readHumidity();    // reading Humidity
  temp = dht.readTemperature(); // read Temperature as Celsius (the default)
}

void printit() {
  Serial.print(rtc.getDOWStr()), Serial.print(" "); // Send Day-of-Week
  Serial.print(rtc.getDateStr()), Serial.print(" -- "); // Send date
  Serial.println(rtc.getTimeStr());    // Send time
  Serial.print("LDR:    "), Serial.println(LDRValue);
  Serial.print("temperature:    "), Serial.println(temp);
  Serial.print("humidity:    "), Serial.println(humid);
  Serial.print("pm25:    "), Serial.println(Pm25);
  Serial.print("pm10:    "), Serial.println(Pm10);
  Serial.print("air pollution level is "), Serial.println(pollutelevel);
  Serial.println("-------------------------------------------------------------------------------- ");
}

void readLDR() {
  LDRValue = analogRead(LDRPin); // read the value from the sensor
}

void printDirectory(File dir, int numTabs) {
  while (true) {
    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (!entry.isDirectory()) {
      // files have sizes, directories do not
      //Serial.print("\t\t");
      //Serial.println(entry.size(), DEC);
      Serial.println();
      delay(500);
      while (entry.available()) {
        Serial.write(entry.read());
      }
      entry.close();
      delay(100);
      char f[20];
      sprintf(f, "/pmbox/%s", entry.name());
      if (DELOLDFILE) {
        SD.remove(f);
      }
    } else {
      entry.close();
    }
  }
}


//print log data to serial for pc upload
void uploadlogdata() {
  File root = SD.open("/pmbox/");
  printDirectory(root, 0);
}

void loop(void) {
  if (Serial.available() > 0) {
    char input_data[3];
    for (int i = 0; i < 3; i++) //data recv
    {
      input_data[i] = Serial.read();  delay(2);
    }
    if (input_data[0] == 49 && input_data[1] == 48 && input_data[2] == 49) {
      Serial.println("101OK");
      delay(5000);
      uploadlogdata();
      delay(500);
      Serial.println("101END");
    }
  }

  bool newData = false;
  for (unsigned long start = millis(); millis() - start < 1000;) //wait 1 second for gps signal
  {
    while (GpsSerial.available())
    {
      char c = GpsSerial.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)) // Did a new valid sentence come in?
        newData = true;
    }
  }

  if (newData)
  {
    int year1;
    byte month1, day1, hour1, minute1, second1, hundredths;
    unsigned long chars;
    unsigned short sentences, failed_checksum;
    float latitude, longitude;

    gps.f_get_position(&latitude, &longitude);
    loc[0] = latitude;
    loc[1] = longitude;
    loc[2] = gps.f_altitude();
    Serial.print("Lat/Long: ");
    Serial.print(latitude, 6);
    Serial.print(", ");
    Serial.println(longitude, 6);
    gps.crack_datetime(&year1, &month1, &day1, &hour1, &minute1, &second1, &hundredths);
    Serial.print("Date: "); Serial.print(month1, DEC); Serial.print("/");
    Serial.print(day1, DEC); Serial.print("/"); Serial.print(year1);
    Serial.print(" Time: "); Serial.print(hour1, DEC); Serial.print(":");
    Serial.print(minute1, DEC); Serial.print(":"); Serial.print(second1, DEC);
    Serial.print("."); Serial.println(hundredths, DEC);
    Serial.print("Altitude (meters): "); Serial.println(loc[2]);
    Serial.print("Course (degrees): "); Serial.println(gps.f_course());
    Serial.print("Speed(kmph): "); Serial.println(gps.f_speed_kmph());
    Serial.print("Satellites: "); Serial.println(gps.satellites());
    Serial.println();
    gps.stats(&chars, &sentences, &failed_checksum);

    Time tm_gps;
    tm_gps.hr = hour1;
    tm_gps.min = minute1;
    tm_gps.sec = second1;
    tm_gps.yr = year1;
    tm_gps.mon = month1;
    tm_gps.date = day1;
    timestamp_gps = rtc.getUnixTime(tm_gps);

    //set date and time
    if (timestamp_local > 0 && abs(timestamp_rtc - timestamp_gps) > 5000) {
      //Serial.print(abs(timestamp_rtc-timestamp_gps)), Serial.print(','), Serial.print(timestamp_rtc), Serial.print(','), Serial.println(timestamp_gps);
      unsigned long t_local_gps = timestamp_gps + tmzone * 3600;
      Serial.println("reset rtc");
      rtc.setTime(hour(t_local_gps), minute(t_local_gps), second(t_local_gps));
      rtc.setDate(day(t_local_gps), month(t_local_gps), year(t_local_gps));
      rtc.setDOW(dayofweek(t_local_gps));
    }
    //Serial.print(timestamp_local), Serial.print(','), Serial.print(timestamp_rtc), Serial.print(','), Serial.println(timestamp_gps);
  } else {
    Serial.println("Invalid or missing gps signal");
  }

  readLDR();
  readtime(); delay(100);
  readtemphumid(); delay(100);
  if (LDRValue < 0 || tm_cur.hr < 0 || tm_cur.hr > 24) {
    HIBERNATE = true;
  } else {
    HIBERNATE = false;
  }

  BTSerial.flush();
  if (!HIBERNATE) {
    digitalWrite(PMSET, HIGH);
    ProcessSerialData_liudu();
    char buf[100];
    sprintf(buf, "#PM25%d+PM10%d+TEMP%d+PLVL%d~\n", Pm25, Pm10, temp, pollutelevel);
    BTSerial.write(buf);
    Serial.write(buf);
    if (pollutelevel == 0) {
      digitalWrite(GREEN, HIGH);
      digitalWrite(RED, LOW);
    } else if (pollutelevel == 1) {
      digitalWrite(GREEN, HIGH);
      digitalWrite(RED, HIGH);
    } else {
      digitalWrite(GREEN, LOW);
      digitalWrite(RED, HIGH);
    }
    delay(500);
    if (haveSD) {
      datalogger();
    }
    delay(500); // delay 5 seconds
  } else {
    digitalWrite(PMSET, LOW);
    digitalWrite(RED, HIGH);
    digitalWrite(GREEN, LOW);
    pollutelevel = -1;
    Pm25 = -1;
  }

  printit();

  // picture loop
  u8g.firstPage();
  do {
    draw();
  } while ( u8g.nextPage() );

  // rebuild the picture after some delay
  delay(15 * 1000); // delay 5 seconds*/
}

//given unix time t, returns day of week Sat-Sun as an integer 1-7
uint8_t dayofweek(unsigned long t)
{
  uint8_t res = ((t / 86400) + 4) % 7;
  if (res)
    return res;
  else
    return 7;
}
